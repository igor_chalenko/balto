if (NOT TARGET Catch2::Catch2)
    set(CPM_DOWNLOAD_ALL true)
    CPMAddPackage(gh:catchorg/Catch2@3.1.0)
endif()