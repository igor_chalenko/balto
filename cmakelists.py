import os
from pathlib import Path

from yaranga.objects.sourcefile import SourceFile
from yaranga.server.context import CMakeContext
from yaranga.server.httptransport import HttpTransportConfig
from yaranga.server.vm import VirtualMachine
from yaranga.state.intercept import Interceptor


class BaltoBootstrap(object):
    @staticmethod
    def catch_tests(cmake: CMakeContext):
        for root, dirs, files in os.walk(Path(cmake.current_source_dir, 'tests')):
            for file in files:
                path = Path(file)
                tgt = cmake.add_executable(name=path.stem, sources=f'tests/{file}')
                cmake.target_include_directories(path.stem, public=['include'])
                cmake.target_link_libraries(path.stem, public='Catch2::Catch2WithMain')
                prp = cmake.get_target_property(tgt.name, 'COMPILE_FEATURES')
                print(f'target.compile_features = {prp}')

                cmake.function('catch_discover_tests', path.stem)
                # we don't have tests after this, because `catch_discover_tests` enumerates
                # tests in a post-build action
                name = cmake.get_test_property('test1', 'NAME')
                print(f'test1.name = {name}')
                print(f"non_existent = {tgt['non_existent']}")
                print(f"non_existent_var = {cmake['non_existent_var']}")
                source: SourceFile = cmake.get_source_file(f'tests/{file}')
                print(f'source.abstract = {source.abstract}')
                print(f'labels = {tgt.source_dir.labels}')
                name = cmake.get_target_property('lib', 'NAME')
                print(f'external target lib: {name}')

    @staticmethod
    def load_catch2_via_cpm(cmake: CMakeContext):
        cmake['_version'] = 'v3.1.0'
        cmake.include('cmake/build-modules/catch2.cmake')
        catch2_source_dir = cmake.get_cache_value('Catch2_SOURCE_DIR')
        print(f"Catch2_SOURCE_DIR = {catch2_source_dir}")
        if catch2_source_dir:
            cmake.include(f'{catch2_source_dir}/extras/Catch.cmake')
            BaltoBootstrap.catch_tests(cmake)
        else:
            raise ValueError('Catch2_SOURCE_DIR is not defined')


def run(cmake: CMakeContext):
    cmake['CMAKE_CXX_STANDARD'] = '20'

    with Interceptor(cmake):
        cmake.include('cmake/GetCPM.cmake')
        initialized = cmake.globals['CPM_INITIALIZED']
        print(f'CPM_INITIALIZED = {initialized}')
        # self.load_catch2_via_cpm(cmake)
